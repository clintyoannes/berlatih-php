<!DOCTYPE html>
<html lang="en">

<html>
<head>
  <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Tukar Besar Kecil</title>
</head>

<body>
<?php
function tukar_besar_kecil($string){
    $besar = range('A','Z');
   $kecil = range ('a','z');

     for($i = 0; $i < strlen($string);$i++){
         $c = 1;
         for ($j = 0; $j < 26; $j++){
             if($string[$i] == $besar[$j]){
                 echo $kecil[$j];
                 $c = 0;
                 break;
             }
             if($string[$i] == $kecil[$j]){
                 echo $besar[$j];
                 $c = 0;
                 break;
             }
         }
         if ($c == 1){
             echo $string[$i];
         }
     }  echo "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>

</body>
</html>