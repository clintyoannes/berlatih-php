<!doctype html>

<html>
<head>
  <meta charset="utf-8">

  <title>Tentukan Nilai</title>

</head>

<body>
<?php
function tentukan_nilai($number)
{
    if($number >= 90){
        echo "Sangat Baik<br>";
    }

    if(70 <= $number && $number < 90){
        echo "Baik<br>";
    }

    if(50 <= $number && $number < 70){
        echo "Cukup<br>";
    }
        
    if($number < 50 ){
        echo "Kurang<br>";
    }
        
   
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>

</body>
</html>